import sys
import re
import gestionDesMots
import gestionCalcule

#Commande pour rouler le programme
#main.py 5 utf8 GerminalUTF8.txt LesTroisMousquetairesUTF8.txt LeVentreDeParisUTF8.txt

def main():
    try:
        fenetre = ""
        encodage = ""
        chemin1 = ""
        chemin2 = ""
        chemin3 = ""

        if len(sys.argv) > 1:
            fenetre = sys.argv[1]
        if len(sys.argv) > 2:
            encodage = sys.argv[2]
        if len(sys.argv) > 3:
            chemin1 = sys.argv[3]
        if len(sys.argv) > 4:
            chemin2 = sys.argv[4]
        if len(sys.argv) > 5:
            chemin3 = sys.argv[5]

        texte = gestionDesMots.lire(encodage, chemin1, chemin2, chemin3)

        dico = gestionDesMots.creation_dico(texte)

        mat = gestionCalcule.create_matrix(gestionDesMots.creation_dico(texte))
        
        gestionCalcule.fill_matrix(mat, dico, texte, fenetre)

        quitter = False
        info_input = ""
        mot_test = ""
        nb_mot_affichage = 10
        nb_methode = 0
        liste_info_input = []

        while quitter == False:
            print("")
            print("Entrez un mot, le nombre de synonymes que vous voulez et la méthode de calcul,")
            print("i.e. produit scalaire: 0, least-squares: 1, city-block: 2 \n")
            print("Tapez q pour quitter.")

            info_input = input("\n")
            liste_info_input = info_input.split()

            nb_methode = -1

            if len(liste_info_input) == 3:
                mot_test = liste_info_input[0]
                nb_mot_affichage = int(liste_info_input[1])
                nb_methode = int(liste_info_input[2])

            if nb_methode < 0 or nb_methode > 2:
                print("Methode incorrect")

            elif len(liste_info_input) == 3:

                print("")
                list_resultats = gestionCalcule.calculer(mat, dico, mot_test, nb_methode, nb_mot_affichage)
                score = 0
                mot = ""

                if len(list_resultats[0]) == 2:
                    for i in range(len(list_resultats)):
                        score, mot = list_resultats[i]
                        print(mot,"-->",score)
                else:
                    print(list_resultats)

            print("")

            if info_input == "q" or info_input == "Q":
                break

            #input("\nAppuyez sur une touche pour continuer...")

    except Exception as e:
        print('Problème!', type(e), e)
        return 0

    return 0

if __name__ == '__main__':
    quit(main())