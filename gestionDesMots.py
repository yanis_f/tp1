import sys
import re

liste = []

def lire(encodage, chemin1, chemin2, chemin3):
    contenu = []
    if chemin1 is not None and len(chemin1) > 0:
        fichier1 = open(chemin1, 'r', encoding = encodage)
        contenu = re.findall(r"\w+",fichier1.read())
        fichier1.close()
    if chemin2 is not None and len(chemin2) > 0:
        fichier2 = open(chemin2, 'r', encoding = encodage)
        contenu = contenu + re.findall(r"\w+",fichier2.read())
        fichier2.close()
    if chemin3 is not None and len(chemin3) > 0:
        fichier3 = open(chemin3, 'r', encoding = encodage)
        contenu = contenu + re.findall(r"\w+",fichier3.read())
        fichier3.close()

    for i in range(len(contenu)):
        liste.extend(contenu[i].split())

    return liste

def creation_dico(liste):
    
    dico = dict()
    j = 0

    for i in range(len(liste)):
        
        if liste[i] not in dico:
            dico[liste[i]] = j
            j = j+1

    return dico