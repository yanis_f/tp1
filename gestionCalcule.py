import sys
import re
import numpy as np

nb_produit_scalaire = 0
nb_least_squares = 1
nb_city_block = 2

def create_matrix(dico):
    matrix = np.zeros([len(dico), len(dico)])
    return matrix

def fill_matrix(matrix, dico, list, fenetre):

    fen = int(fenetre)
    fen = (fen-1)/2
    fen = int(fen)
    
    for j in range(len(list)):
        
        #print(dico.get(list[j]), " ", list[j])
        for k in range(fen):
            
            if k+j+1 >= len(list):
                break
            #print(dico.get(list[j]), " ", list[j] ,"|", dico.get(list[k+j+1]), " ", list[k+j+1])
            matrix[dico.get(list[j]),dico.get(list[k+j+1])] += 1
        
        for h in range((fen*-1),0):
            #print(dico.get(list[j]), " ", list[j] ,"|", dico.get(list[h+j]), " ", list[h+j])
            if h+j >= 0:
                matrix[dico.get(list[j]),dico.get(list[h+j])] += 1

    return 0

def calculer(matrix, dico, mot_test, nb_methode, nb_mot_affichage):
    
    if nb_methode == nb_produit_scalaire:
        calcul_placeholder = produit_scalaire
    elif nb_methode == nb_least_squares:
        calcul_placeholder = least_squares
    elif nb_methode == nb_city_block:
        calcul_placeholder = city_block
    
    if mot_test in dico:
        index_r = dico[mot_test]
    else:
        return "mot inexistant"
    
    v_r = matrix[index_r]
    scores = []
    
    for mot, index in dico.items():
        if index != index_r:
            v = matrix[index]
            score =  calcul_placeholder(v_r, v)
            scores.append((score, mot))

    if nb_methode == nb_produit_scalaire:
        resultats = sorted(scores,reverse=True)
    else:
        resultats = sorted(scores)

    return resultats[:nb_mot_affichage]

def produit_scalaire(u, v):
    return np.dot(u,v)

def least_squares(u, v):
    return np.sum((u-v)**2)

def city_block(u, v):
    return np.sum(np.abs(u-v))